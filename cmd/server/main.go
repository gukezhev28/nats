package main

import (
	"context"
	"time"

	natsmw "github.com/tel-io/instrumentation/middleware/nats/v2"

	"github.com/tel-io/tel/v2"
	"go.uber.org/zap"

	"test-multi-instances-nats/internal/configs"
	testnats "test-multi-instances-nats/internal/test-nats"
	pkg_nats "test-multi-instances-nats/pkg/nats"
)

func main() {
	tele, cc := tel.New(context.Background(), tel.GetConfigFromEnv())
	defer cc()

	// ctx, cancel := signal.NotifyContext(tele.Ctx(), syscall.SIGINT, syscall.SIGTERM, os.Kill)
	// defer cancel()

	ctx, _ := context.WithTimeout(tele.Ctx(), 100*time.Second)

	serviceCfgs, err := configs.LoadServiceCfg()
	if err != nil {
		tel.FromCtx(ctx).Fatal("loading service configs", zap.Error(err)) // FATAL
	}

	natsConn := createNatsConnect(ctx, serviceCfgs.Nats)
	natsConn1 := createNatsConnect(ctx, serviceCfgs.Nats)

	module := testnats.Create(natsConn)
	module.StartTestSub(ctx)

	module1 := testnats.Create(natsConn1)
	module1.StartTestSub(ctx)

	<-ctx.Done()
}

func createNatsConnect(ctx context.Context, natsCfgs configs.NatsCfg) *pkg_nats.Client {
	natsMiddleware := natsmw.New(natsmw.WithTel(tel.FromCtx(ctx).Copy()), natsmw.WithReply(true))

	natsConn, err := pkg_nats.New(ctx, natsCfgs.Addr, natsCfgs.User, natsCfgs.Password, natsMiddleware, pkg_nats.WithDroppedMsgsChecker(2*time.Second), pkg_nats.WithPinger(2*time.Second))
	if err != nil {
		tel.FromCtx(ctx).Fatal("creating nats connect", zap.Error(err)) // FATAL
	}

	return natsConn
}
