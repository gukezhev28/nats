package nats

import (
	"context"
	"time"

	"github.com/tel-io/tel/v2"
	"go.uber.org/zap"
)

func (c *Client) Ping(ctx context.Context) {
	if !c.pinger.turnedOn {
		return
	}
	tLogger := tel.Global().Logger.With(zap.String("module", "nats-pinger"))
	tLogger.Info("Started!!!")
	for {
		select {
		case <-time.Tick(c.pinger.interval):
			tLogger.Sugar().Infof("status: %s", c.conn.Status().String())
		case <-ctx.Done():
			tLogger.Info("STOPPED by context.DONE!!!")
			return
		}
	}
}

func (c *Client) CheckDroppedMsg(ctx context.Context) {
	if !c.droppedMsgschecker.turnedOn {
		return
	}

	tLogger := tel.Global().Logger.With(zap.String("module", "nats-dropped-msgs"))
	tLogger.Info("Started!!!")
	for {
		select {
		case <-time.Tick(c.droppedMsgschecker.interval):
			{
				for _, sub := range c.subs {
					if dropped, err := sub.Dropped(); err == nil && dropped > 0 {
						tLogger.Info("after drop", zap.String("subject", sub.Subject), zap.Int("dropped_msg", dropped))
					} else if err != nil {
						tLogger.Error("after drop", zap.String("subject", sub.Subject), zap.Error(err))
					}
				}
			}
		case <-ctx.Done():
			{
				tLogger.Info("STOPPED by context.DONE!!!")
				return
			}
		}
	}
}
