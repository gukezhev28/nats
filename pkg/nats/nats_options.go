package nats

import "time"

type optFn func(*Client)

func WithPinger(interval time.Duration) optFn {
	return optFn(
		func(c *Client) {
			c.pinger.turnedOn = true
			c.pinger.interval = interval
		},
	)
}

func WithDroppedMsgsChecker(interval time.Duration) optFn {
	return optFn(
		func(c *Client) {
			c.droppedMsgschecker.turnedOn = true
			c.droppedMsgschecker.interval = interval
		},
	)
}
