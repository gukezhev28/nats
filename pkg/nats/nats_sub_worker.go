package nats

import (
	"context"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	natsmw "github.com/tel-io/instrumentation/middleware/nats/v2"
	"github.com/tel-io/tel/v2"
)

func (c *Client) worker(ctx context.Context, handlerFunc natsmw.PostFn, sub *nats.Subscription, subject string) {
	for {
		select {
		case <-ctx.Done():
			{
				tel.FromCtx(ctx).Sugar().Infof("worker -> context.Done subject:%s\n", subject)
				return
			}
		default:
			{
				msgs, err := sub.Fetch(fetchBatch)
				if err != nil {
					if errors.Is(err, nats.ErrTimeout) {
						time.Sleep(5 * time.Second) //wait no messages
						continue
					}
					if errors.Is(err, nats.ErrConnectionClosed) {
						time.Sleep(2 * time.Second) //wait reconnect
						tel.FromCtx(ctx).Sugar().Errorf("connetcion fetching data; subject: %s; err: %s\n", subject, err)
						continue
					}
					tel.FromCtx(ctx).Sugar().Errorf("fetching data; subject: %s; err: %s\n", subject, err)
					continue
				}
				for _, msg := range msgs {
					handler := c.mw.BuildWrappedHandler(func(ctx context.Context, msg *nats.Msg) error {
						go c.handleMessage(ctx, msg, handlerFunc)
						return nil
					})
					handler(msg)
				}
			}
		}
	}
}

