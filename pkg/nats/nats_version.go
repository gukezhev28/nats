package nats

import (
	"fmt"
	"regexp"
)

var natsVersion = regexp.MustCompile(`\d.\d.\d`)

func parseNatsVersion(in string) (major, minor int) {
	findString := natsVersion.FindString(in)
	if findString == "" {
		return -1, -1
	}

	_, err := fmt.Sscanf(findString, "%d.%d.%d", &major, &minor, new(int))
	if err != nil {
		return -1, -1
	}

	return major, minor
}

func natsSupportHeaders(ver string) bool {
	m, vr := parseNatsVersion(ver)

	switch {
	case m < natsMajor:
		return false
	case m == natsMajor && vr < natsMajor:
		return false
	default:
		return true
	}
}
