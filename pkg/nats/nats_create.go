package nats

import (
	"context"
	"strings"
	"sync"
	"test-multi-instances-nats/pkg/jsonitor"
	"time"

	"github.com/nats-io/nats.go"
	natsmw "github.com/tel-io/instrumentation/middleware/nats/v2"
	"github.com/tel-io/tel/v2"

	"go.uber.org/zap"

	"github.com/pkg/errors"
)

const (
	chanLength = 100_000

	maxReconnects = 20

	fetchBatch   = 100
	maxWaitFetch = 3 * time.Second

	defaultTimeout = 15 * time.Second
)

const (
	natsMajor        = 2
	timeOut          = 2 * time.Second
	drainTimeOut     = 30 * time.Second
	pingInterval     = 2 * time.Minute
	maxPingOut       = 2
	reconnectWait    = 2 * time.Second
	maxReconnections = -1
)

const (
	statusHdr    = "Status"
	noResponders = "503"
)

var (
	streamNameInUse = "stream name already in use"
)

type metricParams struct {
	turnedOn bool
	interval time.Duration
}

type Client struct {
	conn      *nats.Conn
	jetStream *natsmw.JetStreamContext
	mw        *natsmw.ConnContext
	subs      []*nats.Subscription

	pinger             metricParams
	droppedMsgschecker metricParams

	sync.Mutex
}

func New(ctx context.Context, addr, user, password string, middleware *natsmw.Core, optFuncs ...optFn) (*Client, error) {
	logger := tel.Global().Logger.With(
		zap.String("module", "nats"),
	)

	logger.Info("Nats info", zap.Any("address", addr), zap.Any("user", user))

	servers := func(url string) []string {
		urls := strings.Split(url, ",")
		for i, s := range urls {
			urls[i] = strings.TrimSpace(s)
		}
		return urls
	}(addr)

	opts := nats.GetDefaultOptions()
	opts.Servers = servers
	opts.Timeout = timeOut
	opts.DrainTimeout = drainTimeOut
	opts.PingInterval = pingInterval
	opts.MaxPingsOut = maxPingOut
	opts.Verbose = true
	opts.RetryOnFailedConnect = true
	opts.MaxReconnect = maxReconnections
	opts.ReconnectWait = reconnectWait
	opts.User = user
	opts.Password = password

	opts.AsyncErrorCB = func(nc *nats.Conn, consumer *nats.Subscription, err error) {
		logger.Sugar().Errorf("nats %s AsyncError: %s %v", consumer.Subject, nc.Status(), nc.LastError())
	}

	opts.ReconnectedCB = func(nc *nats.Conn) {
		logger.Error("nats reconnect: " + nc.Status().String())
	}

	opts.DisconnectedErrCB = func(nc *nats.Conn, err error) {
		logger.Warn("nats disconnect status: " + nc.Status().String())
		if err != nil {
			logger.Error("nats disconnect error: " + err.Error())
		}
	}

	opts.ClosedCB = func(nc *nats.Conn) {
		logger.Sugar().Warnf("nats closed status: %s %v", nc.Status(), nc.LastError())
	}

	conn, err := opts.Connect()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	logger.Sugar().Infof("nats connected; status: %s", conn.Status())

	client := &Client{
		conn: conn,
		mw:   middleware.Use(conn),
	}
	client.jetStream, err = client.mw.JetStream(nats.PublishAsyncMaxPending(256), nats.MaxWait(maxWaitFetch))
	if err != nil {
		return nil, errors.WithStack(err)
	}

	client.callOptionFuncs(optFuncs...)

	client.startMetrics(ctx)

	go client.Close(ctx)

	return client, nil
}

func (c *Client) callOptionFuncs(optFuncs ...optFn) {
	for _, optFunc := range optFuncs {
		optFunc(c)
	}
}

func (c *Client) startMetrics(ctx context.Context) {
	go c.Ping(ctx)
	go c.CheckDroppedMsg(ctx)
}

func (c *Client) Close(ctx context.Context) {
	<-ctx.Done()
	if c.conn == nil {
		return
	}

	var count, bytes int
	for _, sub := range c.subs {
		// calculate number of queued messages and queued bytes
		if pMsgs, pBytes, err := sub.Pending(); err == nil {
			count += pMsgs
			bytes += pBytes
		}

		if err := sub.Drain(); err != nil {
			tel.Global().Sugar().Error("Close -> sub.Drain: ", err, sub.Type(), sub.Queue, sub.Subject)
		}
	}

	tel.Global().Sugar().Infof("nats shutdown, pending %d count of messages in %d subscriptions, and messages bytes size %d", count, len(c.subs), bytes)

	if err := c.conn.Drain(); err != nil {
		tel.Global().Sugar().Error("Close -> Drain", err)
	}

	c.conn.Close()
}

func (c *Client) QueueSubscribe(subject, queue string, handlerFunc natsmw.PostFn, useJS bool) error {
	tel.Global().With(
		zap.String("method", "QueueSubscribe"),
		zap.String("queue", queue),
		zap.String("subject", subject),
		zap.Bool("useJS", useJS)).
		Sugar().Infof("start subscribe %s", subject)
	if useJS {
		return c.queueSubscribeJS(subject, queue, handlerFunc)
	}
	return c.queueSubscribeCore(subject, queue, handlerFunc)
}

func (c *Client) handleMessage(ctx context.Context, msg *nats.Msg, handler natsmw.PostFn) {
	out, err := handler(ctx, msg.Subject, msg.Data)
	if err != nil {
		tel.Global().Error("nats -> handleMessage err:", zap.Error(err), zap.String("subject", msg.Subject))
		return
	}
	if msg.Reply != "" && out != nil {
		err := msg.Respond(out)
		if err != nil {
			tel.Global().Error("nats -> handleMessage respond err:", zap.Error(err), zap.String("subject", msg.Subject), zap.String("reply", msg.Reply), zap.String("out", jsonitor.ToJsonString(out)))
			return
		}
	}
}
