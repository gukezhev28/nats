package nats

import (
	"context"
	"fmt"

	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	natsmw "github.com/tel-io/instrumentation/middleware/nats/v2"
	"github.com/tel-io/tel/v2"
	"go.uber.org/zap"
)

func (c *Client) queueSubscribeCore(subject string, queue string, handlerFunc natsmw.PostFn) error {
	sub, err := c.mw.QueueSubscribe(subject, queue, func(ctx context.Context, msg *nats.Msg) error {
		go c.handleMessage(ctx, msg, handlerFunc)
		return nil
	})
	if err != nil {
		tel.Global().Error("nats.go QueueSubscribe() error:", zap.Error(err), zap.String("subj", subject), zap.Bool("useJS", false))
		return fmt.Errorf("%q: %v", subject, err)
	}
	c.subs = append(c.subs, sub)
	return nil
}

func (c *Client) PublishCore(subject string, data []byte) error {
	err := c.mw.PublishWithContext(tel.Global().Ctx(), subject, data)

	return errors.WithStack(err)
}
