package nats

import (
	"context"
	"fmt"

	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	natsmw "github.com/tel-io/instrumentation/middleware/nats/v2"
	"github.com/tel-io/tel/v2"
	"go.uber.org/zap"
)

func (c *Client) queueSubscribeJS(subject string, queue string, handlerFunc natsmw.PostFn) error {
	sub, err := c.jetStream.QueueSubscribe(subject, queue, func(ctx context.Context, msg *nats.Msg) error {
		go c.handleMessage(ctx, msg, handlerFunc)
		return nil
	})
	if err != nil {
		tel.Global().Error("nats.go QueueSubscribe() error:", zap.Error(err), zap.String("subj", subject), zap.Bool("useJS", true))
		return fmt.Errorf("%q: %v", subject, err)
	}
	c.subs = append(c.subs, sub)
	return nil
}

func (c *Client) PullSubscribeJS(ctx context.Context, subject, durable string, streamName string,
	handlerFunc natsmw.PostFn, opts ...nats.SubOpt) error {
	tel.Global().With(
		zap.String("method", "PullSubscribe"),
		zap.String("subject", subject)).
		Sugar().Infof("start subscribe %s", subject)
	if durable != "" && streamName != "" {
		opts = append(opts, nats.Bind(streamName, durable))
	}

	sub, err := c.jetStream.PullSubscribe(subject, durable, opts...)
	if err != nil {
		return errors.WithStack(errors.Wrap(err, fmt.Sprintf("subject: %s; streamName: %s; durable: %s", subject, streamName, durable)))
	}

	c.subs = append(c.subs, sub)

	tel.Global().Sugar().Info("Subscription " + subject + " complete")

	go c.worker(ctx, handlerFunc, sub, subject)

	return nil
}

func (c *Client) PublishJS(subject string, data []byte) error {
	_, err := c.jetStream.JS().Publish(subject, data)

	return errors.WithStack(err)
}
