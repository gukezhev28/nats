package jsonitor

import (
	"fmt"

	jsoniter "github.com/json-iterator/go"
)

var JSON = jsoniter.ConfigCompatibleWithStandardLibrary

func Unmarshal(data []byte, d interface{}) error {
	if err := JSON.Unmarshal(data, d); err != nil {
		return fmt.Errorf("jsonitor DECODE -> %s", err)
	}

	return nil
}

func Marshal(d interface{}) ([]byte, error) {
	data, err := JSON.Marshal(d)
	if err != nil {
		return nil, fmt.Errorf("jsonitor ENCODE -> %s", err)
	}

	return data, nil
}

func ToJsonString(d interface{}) string {
	data, _ := JSON.MarshalIndent(d, "", " ")

	return string(data)
}
