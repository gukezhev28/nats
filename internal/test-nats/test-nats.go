package testnats

import (
	"context"
	"fmt"
	pkg_nats "test-multi-instances-nats/pkg/nats"
)

type Module struct {
	cli        *pkg_nats.Client
	subject    string
	durable    string
	streamName string
}

func Create(natsCli *pkg_nats.Client) *Module {
	return &Module{
		cli:        natsCli,
		subject:    "test-subject-1",
		durable:    "test-consumer-1",
		streamName: "test-stream-1",
	}
}

func (m *Module) StartTestSub(ctx context.Context) {
	if err := m.cli.PullSubscribeJS(ctx, m.subject, m.durable, m.streamName, func(ctx context.Context, sub string, data []byte) ([]byte, error) {
		fmt.Printf("DATA -> %s", data)
		return nil, nil
	}); err != nil {
		panic(err)
	}
}

func (m *Module) StartPublish(ctx context.Context) {
	m.cli.PublishJS(m.subject, []byte("Hello"))
}
