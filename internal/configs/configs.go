package configs

import (
	"github.com/caarlos0/env/v6"
	"github.com/pkg/errors"
)

type (
	NatsCfg struct {
		Addr     string `env:"NATS_ADDR" default:"127.0.0.1:6379"`
		User     string `env:"NATS_USER" default:""`
		Password string `env:"NATS_PASSWORD" default:""`
	}

	ServiceCfg struct {
		Nats NatsCfg
	}
)

func LoadServiceCfg() (cfgs ServiceCfg, _ error) {
	err := env.Parse(&cfgs)

	return cfgs, errors.WithStack(err)
}
